/// <reference types="react-scripts" />

type GroupId = number;

// Workaround for <Select onChange="" /> and <Input onChange="" />
type MuiChangeEvent = ChangeEvent<{ name?: string | undefined; value: unknown; }>

interface QuestionState {
    items: Array<Question>,
    isFetching: Boolean
}

interface Action<Payload> {
    type: string
    payload?: Payload
}

interface GroupsState {
    current: GroupId
    groups: Array<Group>
}

interface GroupHash {
    [key: number]: Array<Question>
}

interface RootState {
    questions: QuestionState
    groups: GroupsState,
    questionsByGroup: GroupHash
}
