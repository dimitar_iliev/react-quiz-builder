import axios from "axios";

import { requestQuestions, receiveQuestionsFailed, receiveQuestions } from "../redux/actions";
import Question from "../redux/types/Question";

export async function loadQuestions(dispatch: Function) {
    dispatch(requestQuestions());
    try {
        const { data } = await axios.get<Array<Question>>('/questions.json');
        dispatch(receiveQuestions(data));
    } catch(exp) {
        dispatch(receiveQuestionsFailed(exp.message))
    }
}