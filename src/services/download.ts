export function download(state: any) {
   const url = "data:application/octet-stream," + encodeURIComponent(JSON.stringify(state));
   window.open(url, "download_iframe");
}