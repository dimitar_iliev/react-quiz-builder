export function getQuestionsByGroup(state: RootState): GroupHash {
    const { groups, questions } = state;

    const groupsHash: GroupHash = groups.groups.reduce((memo, g) => ({...memo, [g.id]: []}), {});
    return questions.items.reduce((memo, q) => ({...memo, [q.group]: [...memo[q.group], q] }), groupsHash);
}