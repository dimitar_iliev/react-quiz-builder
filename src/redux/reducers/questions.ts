import { ADD_QUESTION, UPDATE_QUESTION, REQUEST_QUESTIONS, RECEIVE_QUESTIONS, RECEIVE_QUESTIONS_FAILED } from "../../actionsTypes/questions";
import Question, { QuestionType, NewQuestion, Option } from "../types/Question";

const defaultState: QuestionState = {
    items: [],
    isFetching: false
};

function createQuestion(payload: NewQuestion): Question {
    const id = `${Date.now()}-${Math.ceil(Math.random()*1000)}`;
    const range = { min: 0, max: 100 };
    const type = QuestionType.OPENENDED;
    const options: Array<Option> = [];

    return {
        ...payload,
        id,
        type,
        range,
        options
    };
}

type ReducerAction = Action<Question> | Action<NewQuestion> | Action<string> | Action<Array<Question>>;

export default function(state: QuestionState = defaultState, action: ReducerAction): QuestionState {
    const { payload, type } = action;

    switch(type) {
        case ADD_QUESTION:
            return {
                ...state,
                items: [...state.items, createQuestion(payload as NewQuestion) ]
            };
        case UPDATE_QUESTION:
            const question = (payload as Question);
            return {
                ...state,
                items: state.items.reduce((memo, item) => {
                    if(question.id === item.id) return [...memo, question];
                    return [...memo, item];
                }, [])
            }
        case REQUEST_QUESTIONS:
            return { ...state, isFetching: true };
        case RECEIVE_QUESTIONS:
            const questions = payload as Array<Question>;
            return { ...state, items: questions, isFetching: false };
        case RECEIVE_QUESTIONS_FAILED:
            return { ...state, isFetching: false};
        default:
            return state;
    }
}