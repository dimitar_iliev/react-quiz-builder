import { combineReducers } from "redux";
import questions from "./questions";
import groups from "./groups";

const reducers = combineReducers({
    questions,
    groups
});

export default reducers;

// type ReducerState = ReturnType<typeof reducers>;

// function getQuestionsByGroup(groups: GroupsState, questions: QuestionState): GroupHash {
//     const groupsHash: GroupHash = groups.groups.reduce((memo, g) => ({...memo, [g.id]: []}), {});
//     return questions.items.reduce((memo, q) => ({...memo, [q.group]: [...memo[q.group], q] }), groupsHash);
// }

// function rootReducer(state: ReducerState, action: Action<any>): RootState {
//     const newState = reducers(state, action);
//     return {
//         ...newState,
//         questionsByGroup: getQuestionsByGroup(newState.groups, newState.questions)
//     }
// }

// export default rootReducer;