import { ADD_GROUP, SET_CURRENT } from '../../actionsTypes/groups';
import { Group } from '../types/Group';

const defaultState = {
    current: 0,
    groups: [
        { text: "Main", id: 0 }
    ]
};

let counter = defaultState.groups.length;

export default ((state: GroupsState = defaultState, action: Action<Group>) => {
    switch(action.type) {
        case ADD_GROUP:
            return {
                ...state,
                groups: [...state.groups, {
                    id: counter++,
                    text: action.payload?.text
                }]
            };
        case SET_CURRENT:
            return {
                ...state,
                current: action.payload?.id
            }
        default:
            return state;
    }
})