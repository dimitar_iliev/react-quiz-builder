export interface Option {
    id?: number | string
    text: string
    nextGroup: GroupId | null
}

export interface Range {
    min: number
    max: number
}

export interface NewQuestion {
    text: string
    group: GroupId
}

export default interface Question {
    id: number | string
    text: string
    type: QuestionType
    group: GroupId
    range: Range
    options: Array<Option>
}

export enum QuestionType {
    OPENENDED = 'openended',
    PICKONE = 'pickone',
    RANGE = 'range'
}