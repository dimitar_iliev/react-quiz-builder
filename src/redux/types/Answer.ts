type AnswerValue = string | number | Date | Array<string | number>;

export interface AnswerOption {
    text: string
    value: AnswerValue
}

export enum AnswerType {
    Open = 'open',
    Range = 'range',
    Numeric = 'numeric',
    Date = 'date'
}


export default interface Answer {
    type: AnswerType
    value: AnswerValue
}