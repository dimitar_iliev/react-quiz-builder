import { ADD_QUESTION, UPDATE_QUESTION, REQUEST_QUESTIONS, RECEIVE_QUESTIONS, RECEIVE_QUESTIONS_FAILED } from "../actionsTypes/questions";
import { ADD_GROUP, SET_CURRENT } from "../actionsTypes/groups";

import Question, { NewQuestion } from "./types/Question";
import { Group } from "./types/Group";

export const addQuestion = (payload: NewQuestion): Action<NewQuestion> => ({
    type: ADD_QUESTION,
    payload
});

export const updateQuestion = (payload: Question): Action<Question> => ({
    type: UPDATE_QUESTION,
    payload
})

export const requestQuestions = (payload: void): Action<void> => ({
    type: REQUEST_QUESTIONS
});

export const receiveQuestions = (payload: Array<Question>): Action<Array<Question>> => ({
    type: RECEIVE_QUESTIONS,
    payload
});

export const receiveQuestionsFailed = (payload: string): Action<string> => ({
    type: RECEIVE_QUESTIONS_FAILED,
    payload
});

export const addGroup = (payload: Group) => ({
    type: ADD_GROUP,
    payload
});

export const setCurrentGroup = (payload: Group) => ({
    type: SET_CURRENT,
    payload
});