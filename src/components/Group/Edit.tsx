import React from "react"

import { Group } from "../../redux/types/Group"
import ListItem from "@material-ui/core/ListItem/ListItem"
import { Button } from "@material-ui/core"

interface Props {
    group: Group,
    onClick: Function,
    children?: number
}

export default (props: Props) => {
    const setCurrent = () => props.onClick(props.group)

    const text = `[${props.children}] - ${props.group.text}`;

    return <ListItem>
            <Button onClick={setCurrent}>{text}</Button>
        </ListItem>
}