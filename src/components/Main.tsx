import React from "react";
import { connect, DispatchProp } from "react-redux";
import { AppBar, Toolbar, Container, Box, Typography, Grid, Button } from '@material-ui/core';
import Question from "../redux/types/Question";
import { addQuestion, updateQuestion, setCurrentGroup, addGroup } from "../redux/actions";
import { Group } from "../redux/types/Group";
import Sidebar from "./Sidebar";
import QuestionsList from "./Questions/List"
import { getQuestionsByGroup } from "../redux/selectors";
import { loadQuestions } from "../services/questions";
import { download } from "../services/download";

class Main extends React.Component<Props> {

    addGroup(text: string) {
        const { dispatch } = this.props;
        dispatch(addGroup({
            text
        }))
    }

    addQuestion() {
        const { dispatch, groups: { current } } = this.props;
        dispatch(addQuestion({
            text: "[Question]",
            group: current
        }));
    }

    updateQuestion(updated: Question) {
        const { dispatch } = this.props;
        dispatch(updateQuestion(updated))
    }

    setCurrent(group: Group) {
        const { dispatch } = this.props;
        dispatch(setCurrentGroup(group));
    }

    render() {
        const { questionsByGroup, dispatch } = this.props;
        const { groups, current } = this.props.groups;

        const questions = questionsByGroup[current] || [];

        const addGroup = () => {
            this.addGroup(`Group ${groups.length}`);
        }

        const handleChange = (updated: Question) => {
            this.updateQuestion(updated);
        }

        const handleLoadQuestions = () => {
            loadQuestions(dispatch);
        }

        const handleSave = () => {
            download({ questions: this.props.questions, groups});
        }

        return (
            <React.Fragment>
                <AppBar>
                    <Toolbar>
                        <Typography variant="h4">Quiz Builder</Typography>
                    </Toolbar>
                </AppBar>
                <Toolbar />
                <Container>
                    <Box my={2}>
                        <Grid container spacing={0}>
                            <Grid item>
                                <Sidebar
                                    items={this.props.groups.groups}
                                    questionsByGroup={questionsByGroup}
                                    onClick={(g: Group) => this.setCurrent(g)}
                                    onAdd={() => addGroup()}
                                />
                            </Grid>
                            <Grid item>
                                <QuestionsList
                                    items={questions}
                                    groups={groups}
                                    onChange={handleChange}
                                    onAdd={() => this.addQuestion()}
                                />
                            </Grid>
                        </Grid>
                    </Box>
                    <Box>
                        <Button onClick={handleLoadQuestions}>Load all questions</Button>
                        <Button onClick={handleSave}>Save questions</Button>
                        <iframe name="download_iframe" style={{display:'none'}} title="iframe to download application state"></iframe>
                    </Box>
                </Container>
            </React.Fragment>
        );
    }
}

interface ClassName {
    [key: string]: any
}

interface StoreProps {
    questions: Array<Question>
    groups: GroupsState,
    questionsByGroup: GroupHash
}

type Props = StoreProps & DispatchProp<any>;

export default connect((state: RootState): StoreProps => {
    return {
        questions: state.questions.items,
        groups: state.groups,
        questionsByGroup: getQuestionsByGroup(state)
    };
})(Main);