import React from "react";
import { Group } from "../redux/types/Group";
import GroupItem from "./Group/Edit";
import { Paper, List, ListItem, Typography } from "@material-ui/core";
import { Box, Button } from "@material-ui/core";

interface Props {
    items: Array<Group>
    questionsByGroup: GroupHash 
    onClick: Function,
    onAdd: Function
}

export default function(props: Props) {

    const count = (g: Group) => {
        if(g.id === undefined || !props.questionsByGroup[g.id]) return 0;
        return props.questionsByGroup[g.id].length;
    }

    return (
        <Paper elevation={0} variant="outlined">
            <Box mx={2}>
                <Typography variant="h6">Groups</Typography>
            </Box>
            <List>
                {props.items.map(g =>
                    <GroupItem key={g.id} 
                        group={g}
                        onClick={(group: Group) => props.onClick(group)}>
                    { count(g) }
                    </GroupItem>
                )}
                <ListItem>
                    <Button variant="outlined" color="primary" onClick={() => props.onAdd()}>
                        Add Group
                    </Button>
                </ListItem>
            </List>
        </Paper>)
}