import React from "react";
import { Option } from "../../redux/types/Question";
import { List, ListItem, Select, Input, MenuItem, Button, Box } from "@material-ui/core";
import { Group } from "../../redux/types/Group";

interface Props {
    id: number | string
    options: Array<Option>
    groups: Array<Group>
    onChange: Function
}

export default function PickoneInput(props: Props) {
    const { options, groups, onChange, id } = props;

    const addNewOption = () => onChange([...options, { text: '[Option Text]', nextGroup: 0, id: Date.now() }]);

    const handleUpdate = (key: string) => (event: MuiChangeEvent, option: Option) => {
        const newOptions = options.reduce((memo: Array<Option>, o: Option): Array<Option> => {
            return [
                ...memo,
                (o !== option ? o : {
                    ...o,
                    [key]: event.target.value
                })
            ]
        }, []);
        onChange(newOptions);
    }

    const handleOptionTextChange = handleUpdate('text');
    const handleNextGroupChange = handleUpdate('nextGroup');

    return <>
            <List>
                {
                    options.map(o => (
                        <ListItem key={o.id}>
                            <Input key={`${id}-range-input`} value={o.text} onChange={(event: MuiChangeEvent) => handleOptionTextChange(event, o)} />
                            <Select value={o.nextGroup} onChange={(event: MuiChangeEvent) => handleNextGroupChange(event, o)}>
                                {
                                    groups.map(g => (
                                        <MenuItem key={g.id} value={g.id}>{g.text}</MenuItem>
                                    ))
                                }
                            </Select>
                        </ListItem>
                    ))
                }
            </List>
            <Box><Button onClick={addNewOption}>Add new</Button></Box>
        </>
}
