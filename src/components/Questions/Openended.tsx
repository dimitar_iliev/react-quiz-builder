import React from "react";
import { Input } from "@material-ui/core";

interface Props {
    onChange: Function,
    value: string
}

export default function OpenendedInput(props: Props) { 
    return <Input onChange={(event: MuiChangeEvent) => props.onChange(event.target.value)} value={props.value} />; 
}