import React from 'react';
import Question, { QuestionType as types } from '../../redux/types/Question';
import { Input, Box, Select, MenuItem, InputLabel, Grid, makeStyles } from '@material-ui/core';
import RangeInput from "./RangeInput";
import Pickone from "./Pickone";
import OpenendedInput from "./Openended";
import { Group } from '../../redux/types/Group';
import { Option } from '../../redux/types/Question';

interface QuestionListItemProps {
    question: Question
    onChange: Function
    groups: Array<Group>
}

const classes = makeStyles({
    inputLabel: {
        display: 'flex',
        alignItems: 'center',
        marginRight: '3px'
    }
});

export default (props: QuestionListItemProps) => {

    const handleChange = (t: string) => (event: MuiChangeEvent) => {
        props.onChange({
            ...props.question,
            [t]: event.target.value
        });
    };

    const handleRangeChange = (range: Range) => {
        props.onChange({
            ...props.question,
            range
        });
    }

    const handleOptionsChange = (options: Array<Option>) => {
        props.onChange({
            ...props.question,
            options
        })
    }
    const { groups, question } = props;
    const { type, range, options } = question; 
    const handleTextChange = (val: string) => props.onChange({
        ...props.question,
        text: val
    });
    
    return (<Box mx={2} style={{width:'400px'}}>

        <Grid container style={{width:'100%'}} wrap="nowrap">
            <Grid item>
                <OpenendedInput onChange={handleTextChange} value={question.text} />
            { type === types.RANGE && <RangeInput id={props.question.id} min={range.min} max={range.max} onChange={handleRangeChange} /> }
            { type === types.PICKONE && <Pickone id={props.question.id} groups={groups} options={options} onChange={handleOptionsChange} /> }
            </Grid>
            <Grid item style={{width:'100%'}}>
                <Grid container justify="center">
                    <InputLabel className={classes().inputLabel}>Question Type:</InputLabel>
                    <Select value={props.question.type} onChange={handleChange('type')}>
                        {
                            Object.values(types).map(qt => <MenuItem  key={qt} value={qt}>{qt}</MenuItem>)
                        }
                    </Select>
                </Grid>
            </Grid>
        </Grid>
    </Box>);
}