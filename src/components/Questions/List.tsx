import React from "react";
import Question from "../../redux/types/Question";
import QuestionItem from "./Edit";
import { Box, Typography, List, ListItem, Button } from "@material-ui/core";
import { Group } from "../../redux/types/Group";

interface Props {
    items: Array<Question>
    groups: Array<Group>
    onChange: Function
    onAdd: Function
}

export default (props: Props) => {

    const list = () =>
        props.items.map(q => {
            return <QuestionItem groups={props.groups} key={q.id} question={q} onChange={(val:string) => props.onChange(val, q)} ></QuestionItem>
        })

    return (<>
        <Box mx={2}>
            <Typography variant="h6">Questions</Typography>
        </Box>
        <List>
            {list()}
            <ListItem>
                <Button variant="outlined" onClick={() => props.onAdd()} color="primary">Add Question</Button>
            </ListItem>
        </List>
    </>)
}