import React, { ChangeEvent } from "react";
import { Grid, InputLabel, Input } from "@material-ui/core";

interface Props {
    id: string | number,
    min: number
    max: number,
    onChange: Function
}

export default function(props: Props) {
    const { min, max, id } = props;
    
    const handleMinChange = (event: ChangeEvent<HTMLInputElement>) => {
        const val = parseFloat(event.target.value);
        props.onChange({
            min: val,
            max: val > max ? val + 1 : max
        });
    }

    const handleMaxChange = (event: ChangeEvent<HTMLInputElement>) => {
        const val = parseFloat(event.target.value);
        props.onChange({
            max: val,
            min: min > val ? val - 1 : min
        });
    }

    return (<Grid container wrap="nowrap">
                <Grid item><InputLabel>Min value:</InputLabel><Input key={`${id}-min-input`} value={min} onChange={handleMinChange} /></Grid>
                <Grid item><InputLabel>Max value:</InputLabel><Input key={`${id}-max-input`} value={max} onChange={handleMaxChange} /></Grid>
            </Grid>);
}